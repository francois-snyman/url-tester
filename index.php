<?php
    $testing = isset($_POST['urls']) && !empty($_POST['urls']);
    if($testing){
        include_once('url-tester.php');
    }
?>

<!DOCTYPE html>
<html>
    <head>
    <!-- http://localhost/SILO-scripts/url-tester.php     -->
        <meta charset="utf-8">
        <title>URL Tester</title>
        <link href="style.css" rel="stylesheet" media="all"/>
        <script type="text/javascript" src="script.js"></script>
    </head>
    <body>
        <div class="headerCont">
            <h2>Test URLs:</h2>
        </div>

    <?php if(!$testing): ?>
        <form id="url-form" class="url-form" action=" " method="post">
            <div class="form-group">
                <textarea id="url-input" name="urls" autofocus onkeypress="enterpress(event)"></textarea>
            </div>
            <div class="form-group submit-container">
                <button id="submit-button" type="submit" name="submit">TEST</button>
            </div>
        </form>
    <?php else: ?>
        <div class="submit-container">
            <button type="button" onclick="location.replace(location.href);" name="reload">RESET</button>
        </div>

        <!-- Successful URls -->
        <h3>Successful:</h3>
        <ul>
            <?php foreach ($successful as $test): ?>
                <li><?=$test['url']. " (".$test['res'].")"?></li>
            <?php endforeach; ?>
        </ul><hr />

        <!-- Redirected URls -->
        <h3>Redirected:</h3>
        <ul>
            <?php foreach ($redirected as $test): ?>
                <li><?=$test['url']. " (".$test['res'].")"?></li>
            <?php endforeach; ?>
        </ul><hr />

        <!-- Restricted URls -->
        <h3>Denied:</h3>
        <ul>
            <?php foreach ($restricted as $test): ?>
                <li><?=$test['url']. " (".$test['res'].")"?></li>
            <?php endforeach; ?>
        </ul><hr />

        <!-- Failed URls -->
        <h3>Unaccessible:</h3>
        <ul>
            <?php foreach ($failed as $test):?>
                <li><?=$test['url']. " (".$test['res'].")"?></li>
            <?php endforeach; ?>
        </ul><hr />
    <?php endif; ?>
    </body>
</html>
