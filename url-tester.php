<?php
    $urls = preg_replace("/[\s,'\"]+/",",", trim($_POST['urls'], ",'\" \n\r"));
    $urls = explode(",", $urls);

    $successful = [];
    $redirected = [];
    $restricted = [];
    $failed = [];

    foreach ($urls as $url) {
        flush();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_FAILONERROR,true);

        $res = curl_exec($ch);

        $tmp = ['url'=>$url,'res'=>substr($res, strpos($res, " ")+1, 3)];
        if( preg_match("/HTTP\/\d+.\d+ 200/", $res) ) {
            $successful[] = $tmp;
        } else if( preg_match("/HTTP\/\d+.\d+ 302/", $res) ) {
            $redirected[] = $tmp;
        } else if( preg_match("/HTTP\/\d+.\d+ 4/", $res) ) {
            $restricted[] = $tmp;
        } else {
            $failed[] = $tmp;
        }
    }
?>
